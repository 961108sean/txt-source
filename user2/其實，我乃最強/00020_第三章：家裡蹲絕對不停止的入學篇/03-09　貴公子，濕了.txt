花了很長時間脫掉上衣，讓施奈達爾覺得白髮女人的胸部更加突出。
作為一個平民，氣質卻十分高尚。
雖然他並沒有讓女人感到不自由，但看那個動作就迷戀了。

手放在皮帶上。包圍的不僅是男性，連女性也屏住呼吸，耳邊響起了金屬的刺耳聲。

雖然褲子滑落了，但是襯衫的長度很長，掛在大腿上。
從下面開始依序解開襯衫的鈕扣。拖延的動作更加緩慢了，施奈達爾變得更加焦急。

「怎麼了，手在顫抖呢。要不讓他們幫忙？」

包圍的一人，體格好的男人露出下流的笑容。

「……不用了，謝謝。」

女子強硬地回答，解開中間的按鈕，這次手指放在最上面的按鈕。一個，又一個解開按鈕。
豐滿的乳溝暴露，終於到了最後一個鈕扣被解開時。

「什麼！？」

突然，施奈達爾的視線變成黑色。

「呀！？」

正在治療他的包圍著的女人發出了悲鳴，之後突然聲音斷了。

「喂，怎麼了？快點開燈！」

沒有人回應他的焦躁。
等不及眼睛習慣黑暗，就先站起來環視四周。

「嗯？」

回頭一看，施奈達爾，發現奇怪的事。

有椅子。

這是自己坐的椅子。確認了那個的存在。舉起左手，也看到了。如果降低視線，自己的服裝也是。
但是，除此之外全都是【黑色】。

不是燈熄了，而是變成了黑暗。正如文字所示，周圍是【染成了黑色】。相反，沒有燈卻能看見，真是很不可思議。

「怎麼回事？喂，有人嗎。有人在嗎？」

雖然不是黑暗，但界線模糊。伸出手，走到剛才還有白髮女人的地方，卻被牆壁擋住了。漆黑的牆壁。

「到底是怎麼回事……？該死。」

啪嗒一聲摸著牆壁，拳頭敲了。

可惡。
「呀！？」

烏黑的頭從牆上冒出來，施奈達爾跳了回去。
全身黑衣的男人出現，背景的黑像是溶化一樣。

「不好意思。我在那邊耽擱了一下。我叫她穿上衣服，她卻一直問我【你是誰】、【這是什麼】之類的問題。那傢伙，真不明白立場啊。」

與這種異常情況不相稱的輕浮情緒。好幾個重疊的聲音讓我感到不快。

「哎呀。忘了換模式了。呃……讓你久等了。」

「你是誰？」

「我是陰暗的英雄，黑暗的正義執行人，【修瓦魯茲．克里格爾】，又名【濕婆】。」

「你，你這傢伙……。你是怎麼侵入這個房間的？你知道我是哈芬侯爵家的下任當家施奈達爾嗎？」

施奈達爾一邊忍受著右肩的疼痛，一邊快速地詠唱。

「好慢啊。抓住你到這牢裡的時候，準備就已經完成了。事到如今，任何攻擊或防禦都沒有用。」

你在說什麼？一邊抱著疑問一邊繼續詠唱，

「——唔！」

右肩劇烈疼痛。試著壓住肩膀，想理解黑衣男的話。

「這是什麼……？」

但還不至於理解情況。
右肩上貼著透明的石頭。前後，拳頭大的什麼？它們在壓碎右肩的傷口上。

黑衣男豎了三根手指。

「我判斷你有三個罪——。」

「回答我的問題！這到底是怎麼回事？你是什麼人——啊啊啊啊啊啊啊！！」

右肩被前後推動，施奈達爾痛到四處打滾。

「那個，不是我還在講嗎？聽到最後啦。還有，我不打算回答你的問題。」

石頭之類的東西鬆動。但是由於劇烈的疼痛而無法詠唱，看不到反擊的機會。

「好吧，我們重新開始。你的第一個罪，是給下級生強加不合裡的因緣引起騷動的。那學校到底是怎麼樣了？在那裡進行魔法戰的不法地帶嗎？

現在，等待疼痛平息，應該有反擊的機會吧。

「……高年級生【指導】下級生是被認可的。我是哈芬家的下一任主人。同時也是副學生會長。那種程度是在允許的範圍內。」

我撒謊了。
不只是校內，校外學生間的魔法戰也被禁止的。但那得看做法了。在事後處理上，在政治力量下都變得無關緊要。直到現在。

「這就是我不喜歡貴族的原因了。總之，我不會原諒你的。必須要有安全的學校生活。」

黑衣男似乎不再隱藏意圖了，用手指交叉。

「那麼，第二個是決鬥的事情。雖然從狀況上來看可以分擔痛苦，但是把貴族的面子帶到弱小的新生身上公開處刑。太髒了，這不行，在公序良俗上有罪。」

「愚蠢，那是為了守護貴族的驕傲——。」

「所以啊，現在由我來審判。用我的基準，可以嗎？如果我說不行。」

「這麼蠻橫的話！」

「你也做過類似的事情吧？別換了立場就發牢騷，太難看了？」

「這……我……」

剛結束了反駁，就把話吞下去了。現在是忍耐的時候，而不是得罪黑衣男。

「那麼，第三個呢，可以說你嗎，不能性騷擾。對女孩子說跳裸體舞更好之類的話，你不覺得羞恥嗎？」

「你和那個女人有關係嗎？」

「不知道。連她的名字都不知道。」

這麼說來，施奈達爾現在還抱著疑問。

在平民中能夠進入格蘭菲爾特特級魔法學院的，大多數都是有實力的人物。而且與出眾的外表，和與不同的性格。
自己沒有聽過，是因為成績不及格嗎？

「話說回來，基於以上三項罪行，讓你暫時安上這個【萬力阻擋】。如果打算取下，就算做了治療也馬上鑽進去，要小心。啊，止血是可以的，所以放心吧。」

「什麼，什麼……？」

「啊，對了。你自己即使想使用魔法也會鑽進去。那樣的話決鬥是不可能的吧？趁現在撤回吧。別丟臉了？」

施奈達爾暫時放心了。在腦內反復回味男人的話，禁不住笑起來了。

「哈哈，哈哈。真愚蠢。雖然不知道你使用什麼魔法，但你以為你擁有能維持數日的魔力嗎？」

即使是簡單的結界魔法，就算只堅持一天，大部分的人都會魔力耗盡。如果是保護王宮的大規模的結界，也要數十人交替才能繼續維持住。

「如果覺得是我在騙你的話，明天早上你也可以試試。在我解開之前，你一直都是這樣。」

騙人，但我沒有說出口。
黑衣男操縱著我從未見過的魔法，自信滿滿地宣告的話，在腦海裡迴盪。

但是——。

（不承認。我絕對不會承認的！）

魔法使被封印魔法的屈辱。
在沒有理由的情況下，被一個不認識的黑衣男屈服是無法忍受的。

不，魔法並沒有完全被封印。
黑衣男沒有說【不能用】。

所謂痛楚，就是忍耐與抗拒。
於是，施奈達爾開始小聲詠唱。

「啊啊啊啊啊!你、你、你！」

劇痛襲擊了右肩，遍佈全身。施奈達爾在地板上滾動。

「才剛說完，你想幹什麼？嘛，這樣就會明白了吧？你先老實了一陣子。如果好好反省的話，會幫你解除的。」

說完，黑衣男消失在出來時的黑牆上。


在黑暗的世界中，施奈達爾橫躺著。
作為大貴族的下期當主，在國內第一的魔法學校擔任副學生會長的自己，流下體液躺在地上。
糟糕。太不像話了。
這是誰都看不到的樣子。一個人在黑暗的世界裡，現在甚至感到安心。

但是——。

時間過了多少呢？
五分鐘，一個小時，也許還不到一分鐘。

黑色的世界突然消失了，就像出現時那樣。

「施奈達爾大人！」
「請您放心！」
「發生什麼事？」
「如此憔悴……」
「要馬上治療，快點！」

啊啊，暴露了不想被看到的一面。只是，不用說黑衣男，連白髮的女人也不在了。

被包圍的人抱起。
其中一人在施奈達爾的下半身發現了異常。

「濕了嗎？」
「誒，這個或許……」
「漏尿了嗎……？」
「施奈達爾大人……」

在不融洽的氣氛中，一旁的女人幫忙治療他的右肩。詠唱之後，手掌微微泛著光芒。

「啊啊啊啊！喂，住手！馬上停止治療！」

推開女人，弄濕胯下趴在地板上。

過於悲慘的樣子，雖然讓包圍的人們感到困惑，但還是有人露出了乾澀的笑容。

「喂，別說了。」
「失禮了！」
「可是……」

悄悄話的聲音，讓自己的心情覺得更加淒慘。

（醜態畢露啊……）

已經連憤怒都不湧出，施奈達爾的意識沉入了黑暗的深處——。