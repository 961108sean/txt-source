「歐⋯⋯」

卵殻完全碎掉了，讓莉雅的全身都露了出來。
不過跟上一次不同，沒有全裸。上一次是因為身上的衣服都被獨眼巨人的胃液給腐蝕掉了。

但即便如此，薩基還是大聲喊了出來！

「胸部！！」

莉雅上半身的皮甲碎裂開來，讓胸部大方的露了出來。

「總、總覺得有點難受⋯⋯」

莉雅發出哀鳴，但這是沒辦法的事。

「莉、莉雅醬總覺得好像長大了？」
「欸欸？」

伸展了一下手腳，衣服很明顯的變小件了。
胸部感到很難受，完全無法忍受的難受。

「到底怎麼回事⋯⋯」

站起來後發現視線也變得有點不同，參考了瑪露跟薩基的視線高低後。

「長高了約半個頭吧？」

上一次變成卵後身體也有成長，但這一次成長的幅度更加顯著。
最重要的還是胸部勒得很痛。

「唉⋯⋯」

莉雅嘆了口氣，胸部太大的話反而會影響戰鬥阿。

「完全是⋯⋯⋯造成我的困擾阿⋯⋯」

莉雅很有氣勢的將全身衣服脫光，
雖然看向巴魯斯的眼神裡帶有著些許怨恨，但是並沒有因此討厭巴魯斯。
利用創世魔法製作出新的盔甲及衣服，不過尺寸調整非常惱人。

「胸部耶！是胸部阿！」

薩基手舞足蹈的持續喊著胸部，不過莉雅對此沒有特別的反應，就當成是撒必死場景吧。

「嗚～嗯。果然尺寸不夠大，穿起來很難受阿」
「胸罩改用運動胸罩那種型式會比較好吧？」
「說的也是⋯⋯⋯為什麼你對女性內衣這麼了解阿？」

故意引導薩基轉頭看向其他地方後，莉雅迅速的整理好服裝儀容，接著製作出一面全身鏡。

「歐⋯⋯」

身高大約是１７０公分，一頭長髮幾乎接近腰部。
細長的睫毛跟眼睛看起來很清秀，瞳孔的顏色會隨著光線而變化，閃耀著黑色及金色。
整體而言，身體變得更加成熟了，但仍然保留了少女特有的韻味。

「好漂亮⋯⋯」

莉雅就這樣看著鏡子裡的自己陶醉了。

「像這樣的美女為什麼會是我自己勒⋯⋯」

莉雅表現出非常悔恨的樣子，看來已經沒救了。

───

就在一團混亂中決定接受巴魯斯的請求。
順便詢問了自己前世愛刀虎徹的下落，結果刀完整的回到自己手上讓莉雅心情平穩了下來。

「所以說，要帶著這個孩子一起走？」

莉雅一副氣勢凌人的樣子面對巴魯斯，之前對於竜神的敬意已經消失到九霄雲外去了。
如果要殺我就給妳殺吧，突然變成了這種態度。

「嗯，她也會成為妳的助力吧」

隨著莉雅的視線看過去，站在那的是一名美少女。

那是先前的幼竜變化成的。因為一般而言是不可能帶著一條竜進入城市裡面的，對此巴魯斯輕鬆的就將幼竜的姿態轉變成人形。
年紀看起來大約１０歳，不過正確的年齡連巴魯斯都不清楚。因為對於竜族而言１０年或２０年給她們的感覺都是『最近』，所以這也是沒辦法的事。

金色的捲髮，淺綠色的瞳孔，一雙大眼睛看起來非常可愛。
作為防護用穿在身上的盔甲是利用奧里哈鋼做的全套鈑金甲。
沒錯，就是利用奧里哈鋼製作出來的。這是做為母親擔心小孩安危而製作出的防具，屬於神器級的防具。
她背在背上的是一把連奧加都難以使用的大劍，而且材質也是奧里哈鋼。看來神竜也不懂得自重阿。

「那麼，她的名字是？」

莉雅嘆氣的同時也等於是同意了帶幼竜一起行動這件事。嘛，應該不至於成為累贅吧。

「阿，還沒有決定。妳作為姊姊幫她取個名字吧」

之後這件事情變得很麻煩。

說起來，莉雅根本沒有命名的才能。
松風跟魯道夫都是把前世的知識拿來利用而已。
如果把自己知道的女性名字拿出來用，這也讓人很難抉擇。

因為自己熟悉的名字都是日本名，完全不適合。而且既然是女性也不能取『清麿』或是『村正』這種名字吧。
雖然也想過歐洲系的女戰士名字，但因為臨終都很悲慘所以駁回。
也想過把這個世界上有名的女戰士名字拿來用，但總覺得配不上竜族而放棄。

這裡還是交給在這方面比較厲害的薩基去煩惱吧。

「欸，我嗎？」

就算莉雅是這樣想的，但這對薩基來說也是個難題。

用遊戲裡的名字這種想法姑且不提，對於隨便都能活上數千年的竜，總不好隨便取個名字吧。
不過實際上薩基提出來的名字，被莉雅一一否決了，這讓薩基很想大喊『乾脆妳自己取算了』

已經不記得提出過多少名字了，薩基就這樣看著那全身鈑金鎧甲，腦袋中浮現出一個名字。

「伊琳娜⋯⋯這個如何？」
「嗯，還不錯。那麼這個名字有什麼故事嗎？」
「那是一位穿著板金鎧甲揮舞大劍戰鬥的女戰士的名字」
「喔，那很好呢」

於是終於決定好名字了。

「好歹也把姓氏一併附上去好了。伊琳娜・庫里蘇托魯，我的妹妹。」
「伊琳娜」
「伊琳娜」
「伊琳娜，恭喜妳」
「伊琳娜，恭喜妳」
「恭喜妳」
「阿，那個阿，歐捏醬，可以也幫我取個名字嗎？」

薩基突然提出自己也想要一個名字。
實際考慮了一下適合薩基的名字後，根據前世江戶時代來說大概會被稱為『與作』或是『八兵衛』這種感覺的名字吧。

「如果是從王族那獲得的名字，就可以堂堂正正的向人宣示了」
「嘛，這倒是無所謂⋯⋯不過以後可以繼續叫你薩基嗎？因為改口很麻煩阿」

看著薩基急忙點頭同意的莉雅，摸著下巴思考了起來。

「薩基⋯薩基⋯薩基斯如何？這是貴族的名字」
「雖然很像是英雄的名字，但聽起來總覺得有點變態的感覺！」

他那強力拒絶的氣勢連莉雅都抵擋不了。

「那麼⋯薩基⋯薩基⋯塔留斯如何？你不是很擅長遠距離攻擊？這跟你還蠻配的」
「薩基塔留斯⋯射手座⋯可以施展流星攻擊，跟我很合拍呢！」

薩基興奮的叫了起來。既不是超人系，也不是宇宙科幻小說的主角，恐怕也沒有千鈞一髮之際拯救人命的能力，但這個名字也不至於完全沒用就是了。

「那麼也順便取個姓吧。就叫作薩基塔留斯・庫里蘇托魯」
「欸？可以嗎？」
「嗯，庫里蘇托魯原本就是很常見的姓氏，只要不要掛上卡薩莉雅就沒事」

薩基很興奮的用手捲著自己的瀏海。

「我是薩基塔留斯・庫里蘇托魯，比較親密的人就叫我薩基吧」

總覺得好像哪裡不太對的感覺。

「那麼伊琳娜，要好好照顧身體歐」
「嗯，巴魯斯醬，我出發囉」

看著女兒很有精神的回答後，巴魯斯的眼睛瞇了起來，接著施展了轉移魔法。

───

再度安靜了下來。

呼，巴魯斯吐著氣，下一瞬間整個空間再度被山一樣的身體給佔滿了。
巴魯斯想起了以往來到這裡的人們，她們大概是繼安娜西亞以來最讓人感到愉快的一群人了。
雖然這條命已經所剩無幾了，但到最後應該都忘不了她們吧。對巴魯斯來說，她很珍惜的這些記憶。

至少她是這麼認為的。
巴魯斯靠著預知能力得知自己一定會再跟伊琳娜及莉雅見面。而到了那個時候，犧牲這個身體進行戰鬥的時刻恐怕也將到來了。

但是預知不是萬能的，像是庫拉利斯被消滅這件事就沒預知到。此外莉雅跟伊琳娜的未來目前也是一片模糊。
等到未來重逢的時刻到來時，也是自己離開這個洞窟的時候了吧，而且恐怕其他三隻神竜也必須再一次現世吧。

不知道能跟這些老友見面嗎，如果能見上一面就好了。

（席法卡不知道還活著嗎？庫歐應該沒問題）

巴魯斯腦中浮現了她們的樣子，接著便進入了淺眠之中。